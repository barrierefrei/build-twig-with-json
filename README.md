# Build Twig with JSON

Add your data as JSON, define routes and templates and just render some Twig to HTML.

## TOC

<!-- TOC -->

- [Build Twig with JSON](#build-twig-with-json)
  - [TOC](#toc)
  - [Develop](#develop)
  - [Setup JSON data](#setup-json-data)
    - [Global data](#global-data)
    - [Sections](#sections)
      - [Section template](#section-template)
      - [Section routing](#section-routing)
      - [Entry slug](#entry-slug)
      - [Entry data](#entry-data)
  - [Integrate into a project](#integrate-into-a-project)

<!-- /TOC -->

## Develop

```bash
npm run build
```

## Setup JSON data

### Global data

Global data is stored in [data/global.json](data/global.json). This data can be accessed in every Twig file.

Example _JSON_:

```json
{
  "siteName": "My Website"
}
```

Example _Twig_:

```twig
<h1>{{ global.siteName }}</h1>
```

Example _HTML_:

```html
<h1>My Website</h1>
```

### Sections

A section has multiple entries.

#### Section template

All entries in a section will be rendered through the same template. The `'@'` in `"@src/index.twig"` in the following example is required. Always put an `'@'` before the path – but do not rename the actual directory (some Gulp related stuff).

```json
{
  "template": "@src/index.twig"
}
```

#### Section routing

Each entry in a section will be rendered as a single _HTML_ file. The route defines the path in the distribution.

```json
{
  "route": "{slug}"
}
```

#### Entry slug

[What is a slug?](https://developer.mozilla.org/en-US/docs/Glossary/Slug).

A slug for the front page can be empty:

```json
{
  "rows": [
    {
      "slug": ""
    }
  ]
}
```

A slug for any other page should not be empty:

```json
{
  "rows": [
    {
      "slug": "foo"
    }
  ]
}
```

The [entry slug](#entry-slug) and the [section routing](#section-routing) combined define the URL of each entry.

Example 1:

```json
{
  "route": "{slug}",
  "rows": [
    {
      "slug": "foo"
    }
  ]
}
```

```
https://example.com/foo
```

Example 2:

```json
{
  "route": "foo/{slug}",
  "rows": [
    {
      "slug": "bar"
    }
  ]
}
```

```
https://example.com/foo/bar
```

#### Entry data

Data for each entry is defined in `data`.

```json
{
  "rows": [
    {
      "data": {
        "foo": "bar"
      }
    }
  ]
}
```

You can access this data in your [section template](#section-template) with `{{ this }}`:

```twig
<p>{{ this.foo }}</p>
```

```html
<p>bar</p>
```

## Integrate into a project

1. Add dependencies to [package.json](package.json)

    ```json
    {
      "devDependencies": {
        "camelcase": "^6.1.0",
        "fs": "^0.0.1-security",
        "gulp": "^4.0.2",
        "gulp-htmlmin": "^5.0.1",
        "gulp-twig": "^1.2.0"
      }
    }
    ```

1. Gulpfile

    Add [gulpfile.js](./gulpfile.js) to your project

    or add from [gulpfile.js](./gulpfile.js) to your existing _Gulpfile_:

    - Imports
      ```js
      const { src, dest } = require('gulp');
      const camelCase = require('camelcase');
      const fs = require('fs');
      const htmlmin = require('gulp-htmlmin');
      const twig = require('gulp-twig');
      ```
    - Function
      ```js
      async function html() {...
      }
      ```
    - Export
      ```js
      exports.html = html;
      ```

1. NPM script

    Call task in _Gulpfile_ with `gulp html` like:

    _package.json_

    ```json
    {
      "scripts": {
        "foo": "gulp html"
      }
    }
    ```

    Call script from _package.json_ via CLI:

    ```bash
    npm run foo
    ```