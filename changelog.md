# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.0.0] - 2022-06-06

### Added

- Initial release

[unreleased]: https://gitlab.com/barrierefrei/build-twig-with-json
<!-- [v0.0.5]: https://gitlab.com/barrierefrei/build-twig-with-json/-/compare/v0.0.0...v1.0.0 -->
[v0.0.0]: https://gitlab.com/barrierefrei/build-twig-with-json/-/tree/v0.0.0
