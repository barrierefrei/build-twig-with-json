const { src, dest } = require('gulp');
const camelCase = require('camelcase');
const fs = require('fs');
const htmlmin = require('gulp-htmlmin');
const twig = require('gulp-twig');

/**
 * HTML
 * @description Adds JSON data to twig files, displays error messages, renders html files, minifies html
 * @dependencies gulp gulp-twig gulp-htmlmin camelcase fs
 */
async function html() {
  /** @type {string} Path to data from project root (same dir as gulpfile) */
  const pathToData = 'data'

  /** @type {string} Path to distribution from project root (same dir as gulpfile) */
  const pathToDist = 'dist'

  /**
   * List files within a directory
   * @param {string} dir Any directory
   * @returns {string[]} A list of files
   */
  const listFiles = (dir) => {
    return fs.readdirSync(dir).filter((file) => {
      return file;
    });
  };

  /**
   * Basename (returns filename without path and extension)
   * @param {string} str Any filename like `'foo/example.com'`
   * @returns {string} Basename of file like `'example'`
   */
  const basename = (str) => {
    let base = new String(str).substring(str.lastIndexOf('/') + 1);
    if (base.lastIndexOf('.') != -1)
      base = base.substring(0, base.lastIndexOf('.'));
    return base;
  };

  /** @type {Object} Available in Twig via {{ global.foo }} */
  let globalData = {};

  // Load global data from file
  globalData = JSON.parse(fs.readFileSync(`${pathToData}/global.json`));

  const sections = listFiles(`${pathToData}/sections`);
  sections.forEach((section) => {
    // Each JSON file in sections is accepted as a section.

    // Check if file is a JSON file
    if (section.split('.').pop() === 'json') {
      // Get contents
      const sectionContents = JSON.parse(
        fs.readFileSync(`${pathToData}/sections/${section}`)
      );

      // A section is structured like this:
      //
      // | name   | rows  | template | route |
      // | ------ | ----- | -------- | ------ |
      // | String | Array | String   | String  |

      /** @type {string} Name: filename (camelCase), required */
      let sectionName;

      /** @type {Object[]} Rows with data, required */
      let sectionRows;

      /** @type {string} Twig template file that will be rendered */
      let sectionTemplate;

      /**
       * @type {String} Route: define the path of the rendered file in the distribution. Can be empty `""` for front page.
       * @example foo/{slug}
       */
      let sectionRoute;

      sectionName = camelCase(basename(section));
      sectionRows = sectionContents.rows;
      sectionTemplate = sectionContents.template.replace('@src', 'src'); // Add namespaces (@see README.md#twig-namespaces);
      sectionRoute = sectionContents.route;

      globalData[sectionName] = []; // section is now available in Twig via {{ global.name }}

      // A row is structured like this:
      //
      // | slug   | data   |
      // | ------ | ------ |
      // | String | Object |

      sectionRows.forEach((entry) => {
        /** @type {string} Id reference */
        let entryId;

        /** @type {Object} Entry data is now available in Twig via {{ entry.foo }} */
        let entryData;

        /** @type {string} Slug */
        let entrySlug;

        // Set entryData
        entryData = entry.data;

        // Set entrySlug
        entrySlug = entry.slug;

        // Set entryId
        entryId = camelCase(entrySlug);
        if (!entrySlug) {
          entryId = 'home'; // the slug for the home page will be empty so we use 'home' as an id reference
        }

        // Set entryRoute
        if (sectionRoute) {
          entryRoute = sectionRoute.replace('{slug}', entrySlug);

          // Add to entryData: route
          entryData = {
            ...entryData,
            route: '/' + entryRoute
          };
        }

        // Add to entryData: id, slug
        entryData = {
          ...entryData,
          id: entryId,
          slug: entrySlug
        };

        // Add entryData to globalData. Each entry is now available in Twig via {{ global.id }}
        globalData[sectionName].push(entryData);

        /**
         * @type {Object} Twig configuration
         * @see {@link node_modules/gulp-twig/index.js}
         */
        const twigConfig = {
          // Add namespaces
          // @see README.md#twig-namespaces
          namespaces: {
            src: `${__dirname}/src`
          },
          data: {
            this: entryData,
            global: globalData
          }
        };

        // Render entry
        if (sectionTemplate) {
          return src(sectionTemplate)
            .pipe(twig(twigConfig))
            .pipe(htmlmin({ collapseWhitespace: true })) // minify html
            .pipe(dest(`${pathToDist}/${entryRoute}`));
        }
      });
    }
  });
}

// Exports
exports.html = html;
